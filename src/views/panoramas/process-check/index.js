export * from './erp-check-strategy'
import CheckFormModal from './check-form-modal.vue'
import CheckViewModal from './check-view-modal.vue'
import ErpLayerDropdown from './erp-layer-dropdown.vue'
import CheckTable from './check-table.vue'
export { CheckFormModal, CheckViewModal, ErpLayerDropdown, CheckTable }

export const FaultOption = {
  comparisonAndExtraction: [
    {
      label: 'A类错误',
      value: 'A',
      children: ['应提取的图斑未提取'],
    },
    {
      label: 'B类错误',
      value: 'B',
      children: [
        '图斑未居中',
        '结构类型判断错误',
        '定位错误',
        '区域范围判断错误',
        '社区界限、区域范围未显示',
      ],
    },
    {
      label: 'C类错误',
      value: 'C',
      children: ['提取了无用图斑', '未发现全景影像质量有问题'],
    },
  ],
  plotting: [
    {
      label: 'A类错误',
      value: 'A',
      children: ['漏画线', '画错地块范围'],
    },
    {
      label: 'B类错误',
      value: 'B',
      children: [
        '漏放飞机',
        '视频未上传或视频上传错误',
        '飞机场景选择错误',
        '错误设置默认视角',
        '未正确设置正北方向',
      ],
    },
    {
      label: 'C类错误',
      value: 'C',
      children: [
        '地图未展示',
        '视频命名错误',
        '未发现视频、全景明显影像质量有问题',
        '遮挡部分未画虚线',
        '未正确设置正北方向',
        '未发现全景影像质量有问题',
      ],
    },
  ],
}
