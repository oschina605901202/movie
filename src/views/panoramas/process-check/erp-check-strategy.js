import { cloneDeep } from 'lodash'
// import { ErpPolygonTypeList } from '../utils/enum'
import { drawReportPolygon } from '@/utils/krpano'

const ProcessEnum = {
  check: {
    borderColor: '#ff00ed',
  },
  checkAudit: {
    borderColor: '#00ffdc',
  },
}

// 一个通用弹窗：查看质检内容
// 一个通用弹窗：保存、编辑质检内容

// 区分 两个质检 check checkAudit 和 处理图层
// 原来各个模块的处理逻辑都不变，做加法
// 不跟模块进行耦合，因为模块的处理图层是不同的，但质检图层是相同的

// 希望有一个类，传入状态，，就可以返回一个对象，用这个对象的功能：
// · 绘制多边形，带有自己特定的颜色、自己特定的点击事件
// · 返回一个通用查询是否质检的状态
// · 增加对 polgyonData 的额外处理

export class ErpCheckStrategy {
  state = '' // 可能为 handle
  polygon = {
    // 当前状态下的多边形要素
    hotspotType: '',
    borderColor: '',
  }
  otherPolygon = {
    // 当前状态下，另一个多边形的要素
    hotspotType: '',
    borderColor: '',
  }
  checkList = []
  hotspotTypeUnion = []
  get isCheck() {
    return ['check', 'checkAudit'].includes(this.state)
  }
  constructor(state, { hotspotType }) {
    this.state = state
    this.hotspotTypeUnion = Object.values(hotspotType)
    if (this.isCheck) {
      this.polygon.hotspotType = hotspotType[state]
      this.polygon.borderColor = ProcessEnum[state].borderColor
      const otherState = state === 'check' ? 'checkAudit' : 'check'
      this.otherPolygon.hotspotType = hotspotType[otherState]
      this.otherPolygon.borderColor = ProcessEnum[otherState].borderColor
    }
  }

  replacePolygonAttr(polygonData) {
    const data = cloneDeep(polygonData)
    data.hotspotType = this.polygon.hotspotType
    data.style.borderColor = this.polygon.borderColor
    return data
  }
  drawSavedPolygon(krpano, polygonData, { onClick, onDelete, onEdit, type }) {
    if (polygonData.hotspotType === this.polygon.hotspotType) {
      // 查询质检状态，若是已整改，那么不允许修改，只能删除
      const t = this.checkList.find((e) => e.hotspotId === polygonData.id)
      const isCorrected = t ? t.processStatus === 1 : false
      drawReportPolygon(krpano, {
        ...polygonData,
        closeBtn: true,
        editBtn: !isCorrected,
        onDelete,
        onEdit,
        onClick,
      })
    } else if (this.isCheckHotspotType(polygonData.hotspotType) && type === '初始化') {
      drawReportPolygon(krpano, {
        ...polygonData,
        closeBtn: false,
        editBtn: false,
        onClick,
      })
    }
  }
  isCheckHotspotType(hotspotType) {
    return this.hotspotTypeUnion.includes(hotspotType)
  }
  // 删除质检
  deleteCheck({ hotspotId, vi }) {
    // vi.$http['delete']('/panoramas/imageSceneHotspot', { data: [hotspotId] })
    // return
    const checkData = this.getCheckData({ hotspotId })
    if (!checkData) return
    return new Promise((resolve, reject) => {
      vi.$confirm(`是否删除质检${this.state === 'checkAudit' ? '审核' : ''}内容`, '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning',
      })
        .then(() => {
          vi.$http
            .delete('/panoramas/inspect', {
              data: {
                idList: [checkData.id],
                erpAccount: vi.$route.query.erpAccount,
              },
            })
            .then(({ data: res }) => {
              if (res.code !== 200) {
                reject()
                return vi.$message.error('删除失败')
              } else {
                resolve()
              }
            })
        })
        .catch(() => {
          reject()
        })
    })
  }
  // 请求质检列表
  requestCheckList({ imageSceneId, module, vi }) {
    return new Promise((resolve, reject) => {
      vi.$http
        .post('/panoramas/inspect/queryInspectListByImageSceneIdAndWorkType', {
          imageSceneId,
          workType: module,
        })
        .then((res) => {
          if (res.data.code == 200) {
            this.checkList = res.data.data
            resolve(this.checkList)
          } else {
            reject()
            vi.$message.error('请求质检信息失败')
          }
        })
        .catch(() => {
          reject()
        })
    })
  }
  // 根据热点id查询质检信息
  getCheckData({ hotspotId }) {
    if (this.checkList.length === 0) {
      console.error('请先使用当前对象查询质检列表')
    }
    const t = this.checkList.find((e) => e.hotspotId === hotspotId)
    return t
  }
}
