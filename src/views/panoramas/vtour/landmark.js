import { HotspotDragState } from '@/utils/krpano/hotspot-drag-state'

export function drawLandmark(
  krpano,
  { id, name, ath, atv, imgSrc, onClick, draggable, onDragEnd, dragHandle }
) {
  // 先画一个容器
  const container = krpano.addhotspot('landmarkContainer' + id)
  container.ath = ath
  container.atv = atv
  container.width = 50
  container.height = 124
  container.type = 'container'
  container.oy = 0 - container.height / 2
  container.hotspotType = '图片集'
  // 在容器里面画线
  const line = krpano.addlayer('landmarkLine' + id)
  line.type = 'html'
  line.width = 2
  line.height = 50
  line.x = container.width / 2 - 1
  line.y = container.height - line.height
  line.bgalpha = 0.8
  line.bgcolor = '0xffffff'
  line.parent = `hotspot[landmarkContainer${id}]`
  // 在容器里显示图片
  const pic = krpano.addlayer('landmarkPic' + id)
  pic.type = 'image'
  pic.width = container.width
  pic.height = 40
  pic.y = line.y - pic.height
  pic.bgborder = '1 0xffffff 0.7'
  pic.bgroundedge = 4
  pic.url = imgSrc || 'icon/image/ty-logo.gif'
  pic.parent = `hotspot[landmarkContainer${id}]`
  pic.onclick = onClick
  // 在容器里写地标名称
  const text = krpano.addlayer('landmarkText' + id)
  text.type = 'text'
  text.html = name
  text.visible = Boolean(name)
  text.parent = `hotspot[landmarkContainer${id}]`
  text.bgcolor = '0x000000'
  text.align = 'top'
  text.css =
    'text-align:center;color:#FFFFFF;font-size:18px;transform:translate(-50%, 0);padding:4px'
  text.bgalpha = 0.7
  text.bgroundedge = 5
  text.x = `calc(25px - 50%)`
  text.onclick = onClick
  // 其他的属性判断
  if (draggable) {
    container.bgcapture = true
    container.cursor = 'grabbing'
    const dragEvent = new HotspotDragState(krpano, container)
    pic.ondown = () => dragEvent.moving({ handle: pic })
    pic.onup = () => dragEvent.finished({ handle: pic })
    // 仅对 pic 进行拖拽
    if (dragHandle !== 'pic') {
      container.ondown = () => dragEvent.moving()
      container.onup = () => dragEvent.finished()
    }
    dragEvent.onChange = () => {
      const p = dragEvent.getLatestPosition()
      onDragEnd(p, dragEvent)
    }
  }
}

export function updateLandmark(krpano, { id, name, imgSrc, onClick }) {
  const text = krpano.layer.getItem('landmarkText' + id)
  if (text) {
    text.html = name
    text.visible = Boolean(name)
  }
  const pic = krpano.layer.getItem('landmarkPic' + id)
  if (pic) {
    pic.url = imgSrc || 'icon/image/ty-logo.gif'
    pic.onclick = () => onClick()
  }
}

export function landmarkDraggable(krpano, id, { onDragEnd }) {
  const container = krpano.hotspot.getItem('landmarkContainer' + id)
  const pic = krpano.layer.getItem('landmarkPic' + id)
  container.bgcapture = true
  container.cursor = 'grabbing'
  const dragEvent = new HotspotDragState(krpano, container)
  pic.ondown = () => dragEvent.moving({ handle: pic })
  pic.onup = () => dragEvent.finished({ handle: pic })
  container.ondown = () => dragEvent.moving()
  container.onup = () => dragEvent.finished()
  dragEvent.onChange = () => {
    const p = dragEvent.getLatestPosition()
    onDragEnd(p)
  }
}

export function clearKrpanoLandmark(krpano, id) {
  krpano.removehotspot('landmarkContainer' + id)
  krpano.removelayer('landmarkPic' + id)
  krpano.removelayer('landmarkLine' + id)
  krpano.removelayer('landmarkText' + id)
}

export function flickerLandmark(krpano, id) {
  const pic = krpano.layer.getItem('landmarkPic' + id)
  const line = krpano.layer.getItem('landmarkLine' + id)
  pic.bgborder = '1 0x3fbd01 0.7'
  line.bgcolor = '0x3fbd01'
  setTimeout(() => {
    pic.bgborder = '1 0xffffff 0.7'
    line.bgcolor = '0xffffff'
  }, 300)
  setTimeout(() => {
    pic.bgborder = '1 0x3fbd01 0.7'
    line.bgcolor = '0x3fbd01'
  }, 600)
  setTimeout(() => {
    pic.bgborder = '1 0xffffff 0.7'
    line.bgcolor = '0xffffff'
  }, 900)
}
