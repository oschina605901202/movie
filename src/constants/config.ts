/**
 * 主题设置默认值
 */
export const themeSetting = {
  sidebar: "dark",
  topHeader: "primary",
  themeColor: "#0052d9",
  navLayout: "left",
  contentFull: true,
  logoAuto: true,
  colorIcon: false,
  sidebarUniOpened: true,
  openTabsPage: true,
  tabStyle: "default",
  sidebarCollapse: false
};
